// jestではrequireしたモジュールは自動でモック化されるので、
//　テストコード内の　require の戻り値は自動的に mock object になるため、実際のmodule　が必要な場合は、必ず unmock する必要があり
jest.unmock('../../src/ReactNativeTesting');

import '../testutils/jest';
// import ReactTestUtils from 'react-dom/test-utils'; // ES6
import ReactNativeTesting from '../../src/ReactNativeTesting';
import ReactNativeTestingChild from '../../src/ReactNativeTestingChild';

// 関数[describe]: テストケースをまとめる
// この中にtest()やit()で作成した関連するテストをまとめていく
// describe()も入れ子にできる
describe('ReactNativeTesting', () => {

  // この一連のテストで使用する変数を用意
  let component;
  let textInput;
  const defaultState = {text: ''};

  /*
　 スナップショットテスト(初期表示テスト)
  */
  // [test(name, fn)]:テストを作成する。第一引数に何のテストであるかを記載し、第二引数にテストの内容を渡す
  // 関数[it(name, fn)]も同様
  test('renders correctly', () => {
    // render.create()に渡されたコンポーネントをjson形式に変換
    const snapshot = renderer.create(<ReactNativeTesting />).toJSON();
    // toMatchSnapshot()を実行すると__snapshota__フォルダが生成され、json形式でコンポーネントが保存される。
    // 保存されたコンポーネントと一致することを確認する
    expect(snapshot).toMatchSnapshot();
  });

　/*
   各テストの実行前に毎回実行する処理の作成
  */
  //　このdescribe()でまとめられたテスト群を実行するために毎回行う処理を用意
  beforeEach(() => {
    // 関数[shallow(comp)]:引数に渡されたコンポーネントをレンダリング
    component = shallow(<ReactNativeTesting />);
    // 関数[find()]:引数に渡された名前のコンポーネントを探して取得
    textInput = component.find('TextInput');
  });

  /*
   コンポーネントのstateの初期値確認
  */
  it('has default state', () => {
    expect(component.state()).toEqual(defaultState);
  });

  /*
   文字列に関するテスト
  */
  it('renders welcome text', () => {
    const expectedText = 'Welcome to React Native testing demo app';
    // 関数[find()]:Textコンポーネントを探して取得
    // 関数[children()]:コンポーネント配下の要素を全て取得
    // 関数[text()]:テキスト要素の文字列を取得
    const text = component.find('Text').children().text();
    expect(text).toEqual(expectedText);
  });

  /*

  */
  it('renders input field with placeholder', () => {
    const expectedPlaceholder = 'write something';
    expect(textInput.length).toBe(1);
    // beforeEachの処理で取得したTextInputコンポーネントのpropsの中からplaceholderを取得
    // ReactNativeTesting.jsのL.33
    expect(textInput.props().placeholder).toEqual(expectedPlaceholder);
  });


  // 【ReactNativeTestingコンポーネントのtextInputに文字が入力された際の挙動のテスト】
  describe('when text changes', () => {

    const newTextValue = 'random string';
    beforeEach(() => {
      // 関数[simulate(event, value)]:DOM上でイベントのディスパッチを行う
      // この時,DOM上で行うため、rendererではなく、shallowを使ってtextInputコンポーネントをレンダリングしておく
      // reactで使えるイベントは全てシュミレートできる
      // 参照)https://facebook.github.io/react/docs/events.html#supported-events
      textInput.simulate('changeText', newTextValue);
    });

    /*
     textInputコンポーネントへの文字入力テスト
    */
    it('updates component state', () => {
      expect(component.state().text).toEqual(newTextValue);
    });

    /*
     子要素であるコンポーネントのテスト
    */
    it('passes text from state to ReactNativeTestingChild', () => {
      // ReactNativeTestingChildコンポーネントを取得
      const childComponent = component.find(ReactNativeTestingChild);
      expect(childComponent.props().text).toEqual(newTextValue)
    });

    // 【ReactNativeTestingChildコンポーネントのtextInputの文字がクリアされた際のテスト】
    describe('when clearText callback is called', () => {
      beforeEach(() => {
        const childComponent = component.find(ReactNativeTestingChild);
        childComponent.simulate('clear');
      });

      it('resets state', () => {
        expect(component.state()).toEqual(defaultState);
      });
    });
  });
});