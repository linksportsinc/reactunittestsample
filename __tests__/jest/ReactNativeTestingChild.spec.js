jest.unmock('../../src/ReactNativeTestingChild');

import '../testutils/jest';
import ReactNativeTestingChild from '../../src/ReactNativeTestingChild';
import * as Utils from '../../src/ReactNativeTestingUtils';

describe('ReactNativeTestingChild', () => {
  let component;
  let onClearStub;
  const capitalizedString = 'mock string value';

  test('renders correctly', () => {
    const stringValue = 'random value';
    // propsに値を設定した状態でレンダリング
    const snapshot = renderer.create(<ReactNativeTestingChild text={stringValue} onClear={onClearStub} />).toJSON();
    expect(snapshot).toMatchSnapshot();
  });

  // テストで使用する関数のモックを用意
  // 関数[mockReturnValue(v)]:テストで使用する関数の戻り値を指定する
  beforeEach(() => {
    Utils.capitalizeWords = jest.fn(); // TODO: ???
    // 実際に関数を実行せず、直接戻り値を指定する=テストに関係ない関数のロジックを考慮する必要がないので
    // テストが失敗した場合、原因を絞りやすい
    Utils.capitalizeWords.mockReturnValue(capitalizedString);
    onClearStub = jest.fn();
  });

　// 【ReactNativeTestingChildコンポーネントの入力値がない場合のテスト】
  describe('when props.text is empty', () => {
    const stringValue = '';
    beforeEach(() => {
      component = shallow(<ReactNativeTestingChild text={stringValue} onClear={onClearStub} />);
    });

    /*
     テキスト未入力時のプレイスホルダー表示テスト
    */
    it('renders placeholder string', () => {
      const expectedString = 'You must input something!';
      // 関数[node(n)]Textコンポーネントが複数あるうち、n+1番目のTextコンポーネントを取得
      // コンポーネントの子要素へはコンポーネント.props.childrenでアクセスできる
      // 子要素にpropsを渡したい場合はReact.cloneElement() を使用する
      // 参照)http://blog.mudatobunka.org/entry/2016/08/14/182333
      expect(component.find('Text').nodes[1].props.children).toEqual(expectedString);
    });

    it('does not call capitalizeWords', () => {
      // _getCapitalizedString()が呼び出されていないことを確認
      // [mockFn.mock.calls]:mockFnが呼び出された回数を返却
      expect(Utils.capitalizeWords.mock.calls.length).toBe(0);
    });


    // 【ReactNativeTestingChildコンポーネントのクリアボタンのテスト】
    describe('when clear text button is pressed', () => {
      /*
     クリアボタンが押下された際の挙動テスト
    */
      it('should call onClear callback', () => {
        component.find('TouchableHighlight').simulate('press');
        expect(onClearStub).toBeCalled();
      });
    });
  });

  // 【ReactNativeTestingChildコンポーネントの入力時のテスト】
  describe('when props.text is not empty', () => {
    const stringValue = 'random value';
    beforeEach(() => {
      component = shallow(<ReactNativeTestingChild text={stringValue} onClear={onClearStub} />);
    });
    
    /*
     テキスト入力時の挙動テスト
    */
    it('renders string with capitalizeWords', () => {
      expect(Utils.capitalizeWords).toBeCalledWith(stringValue);
      expect(component.find('Text').nodes[1].props.children).toEqual(capitalizedString);
    });
  });
});