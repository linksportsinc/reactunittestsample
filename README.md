# ReactUnitTestSample

[react-native-unit-tests](https://github.com/varmais/react-native-unit-tests)を参考に、
Jestのテストコードに対してコメントを追記しました。

このテストコードはiOS/Android、共に動き、ES6を採用しています。

また、Jest以外にもコメントは記載してありませんが、  
AirBnBの同じくUnitTestのライブラリであるEnzymeを使用し、  

別途mochaのテストコードもあります。

blog: [Unit Testing React Native Components with Mocha and Enzyme](http://valuemotive.com/2016/08/01/unit-testing-react-native-components-with-mocha-and-enzyme/).


## Qiitaまとめ記事
- [テスト概要](http://qiita.com/yuri_iOS/items/855609b35cecaeb94abf)
- [Jest準備と概要](http://qiita.com/yuri_iOS/items/9f14d1f1d9f41e37fe4b)
- [非同期処理のテストとモック](http://qiita.com/yuri_iOS/items/7003273ebedf77074d56)
    
    
## Modules used for testing:

- [jest](https://facebook.github.io/jest/)
- [mocha](https://mochajs.org/)
- [sinon](http://sinonjs.org/)
- [chai](http://chaijs.com/)
- [enzyme](http://airbnb.io/enzyme/)
- [babel-plugin-rewire](https://www.npmjs.com/package/babel-plugin-rewire)


## Installation and usage

    $ npm install

    All mocha tests:
    $ npm run mocha

    Single component on mocha watch mode:
    $ npm run mocha:watch ReactNativeTestingUtils

    All jest tests:
    $ npm run jest

    Jest watch mode:
    $ npm run jest -- --watch
