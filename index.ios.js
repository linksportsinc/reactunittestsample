'use strict';

import { AppRegistry } from 'react-native';
import ReactNativeTesting from './src/ReactNativeTesting';

//　register root component
AppRegistry.registerComponent('ReactNativeTesting', () => ReactNativeTesting);
